package com.example.tefa.ui.student

import android.os.Bundle
import com.example.tefa.base.BaseActivity
import com.example.tefa.databinding.ActivityListStudentBinding

class ListStudentActivity : BaseActivity() {
    private lateinit var binding: ActivityListStudentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListStudentBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}