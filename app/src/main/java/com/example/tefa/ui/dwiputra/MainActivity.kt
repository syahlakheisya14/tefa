package com.example.tefa.ui.dwiputra

import android.os.Bundle
import android.widget.Toast
import com.example.tefa.base.BaseActivity
import com.example.tefa.databinding.ActivityMainBinding
import com.example.tefa.utils.IntentKey
import com.example.tefa.utils.load
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var avatar: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initIntent()
        initView()
        initObserver()
        initAction()
    }

    private fun initIntent() {
        // Get all your intent here
        avatar = intent.getStringExtra(IntentKey.KEY_USER).toString()

    }

    private fun initView() {
        // Set your default UI Logic
        binding.ivUserAvatar.load(avatar)
    }

    private fun initObserver() {
        // Init LiveData Observer here
    }

    private fun initAction() {
        // Set interaction between user and UI here
        binding.ivUserAvatar.setOnClickListener {
            Toast.makeText(this, avatar, Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}